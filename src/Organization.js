function createIndex(meta) {
  class Index {
    constructor() {
      this.onCertify = (decision) => decision;
      this.clear()
    }

    clear() {
      this.proposals = {}
      this.decisions = {}
      this.members = new Set(meta.initialMembers)
    }

    /**
     * Propose a decision to vote on.
     */
    proposeDecision(decision, id) {
      this.proposals[id] = decision
    }

    /**
     * Vote on a proposed decision.
     * You can change your vote until a proposal reaches an absolute majority.
     * - Be aware of the network consequences and race conditions this results in.
     */
    voteDecision(id, voterId) {
      if (this.members.has(voterId) && !this.decisions[id] && !this.proposals[id].votes.has(voterId)) {
        this.proposals[id].votes.add(voterId)

        this.count(this.proposals[id], id)
      }
    }

    /**
     * Count votes for a proposal and apply it if  there is
     * sufficient votes.
     */
    count(proposal, id) {
      if (proposal.votes.size > 0.5 * this.members.size) {
        this.decisions[id] = proposal

        this.apply(this.decisions[id])
      }
    }

    apply(decision) {
      switch (decision.type) {
        case "APPLICATION":
          for (let member of decision.members) {
            this.members.add(member)
          }
          break
        case "BAN":
          for (let memberId of decision.members) {
            this.members.delete(memberId)
          }
          break
        default:
          this.onCertify(decision)
          break
      }
    }

    updateIndex(oplog) {
      this.clear()
      let values = oplog.values.slice()

      const handled = new Set()
      for (let item of values) {
        if (handled.has(item.hash)) {
          continue
        }
        handled.add(item.hash)

        switch (item.payload.op) {
          case "PROPOSE":
            let decision = item.payload.decision
            decision.vote = new Set()
            decision.id = item.hash
            this.proposeDecision(decision, decision.id)
            break
          case "VOTE":
            let proposalId = item.payload.id
            let voterId = item.identity.id

            this.voteDecision(proposalId, voterId)
            break
          default:
            break
        }
      }
    }
  }
  return Index
}

class Organization extends Store {
  constructor(ipfs, id, dbname, options) {
    if (!options) options = {}
    if (!options.onCertify) Object.assign(options, {
      onCertify: (x) => x
    })
    if (!options.Index) Object.assign(options, {
      Index: createIndex(option.meta)
    })

    super(ipfs, id, dbname, options)
    this._type = Organization.type
  }

  static get type() {
    return "organization"
  }

  get decisions() {
    return this._index.decisions
  }

  get members() {
    return this._index.members
  }

  get proposals() {
    return this._index.proposals
  }

  propose(decision) {
    return this._addOperation({
      decision: decision
    })
  }

  vote(proposalId) {
    return this._addOperation({
      id: proposalId
    })
  }

  /**
   * Propose to add an identity to the members of the organization.
   */
  applyMember(members = [this.id]) {
    return this.propose({
      type: "APPLICATION",
      members: members,
    })
  }

  /**
   * Proposes to remove an identity from the members of the organization
   */
  banMember(members = [this.id]) {
    return this.propose({
      type: "BAN",
      members: members,
    })
  }
}

module.exports = Organization