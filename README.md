# dOrg

A distributed organization where decisions can be made in an OrbitDB store.

## Organization
An organization has members, it can make decisions, add members and remove them.

- Organization: a group of people that join together to do something.
- Member of an Organization: Some person with the ability to vote on organization decisions.
- Decision: A data that was accepted by an absolute majority of members and can be used to implemented.
- Certification: A decision to 
- Application Decision: A decision to accept new members into an organization.
- Ban Decision: A decision to remove members from the organization.
